package DBWrapper

import (
	"gitlab.com/HuntDownUPC/go-modules/db/backends/mongo/MongoDBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
	"go.mongodb.org/mongo-driver/bson"
)

type BackendDB string

// Initial version of this file is basically a proxy for MongoDBWrapper
// In the future we should do a plugin interface for this
// This package has the goal of abstracting access to the DB backend
func Change_index(info *dbd.Db_Context, index string) {
	MongoDBWrapper.Change_index(info, index)
}

func ValidateSession(fd dbd.Db_id) bool {
	return MongoDBWrapper.ValidateSession(fd)
}

func GetDBBackendName() string {
	return MongoDBWrapper.GetDBBackendName()
}

func GetCollection(fd dbd.Db_id) string {
	return MongoDBWrapper.GetCollection(fd)
}

func GetDatabase(fd dbd.Db_id) string {
	return MongoDBWrapper.GetDatabase(fd)
}

func ProcessFiltersSingle(filter_list []dbd.Filters, query *bson.M) bool {
	return MongoDBWrapper.ProcessFiltersSingle(filter_list, query)
}

func ProcessFiltersMultiple(filter_list []dbd.Filters, query *[]bson.M) bool {
	return MongoDBWrapper.ProcessFiltersMultiple(filter_list, query)
}

func UseDatabase(fd dbd.Db_id, db_name string) (bool, error) {
	return MongoDBWrapper.UseDatabase(fd, db_name)
}

func UseCollection(fd dbd.Db_id, username string) (bool, error) {
	return MongoDBWrapper.UseCollection(fd, username)
}

func GetDBContext(fd dbd.Db_id) (dbd.Db_Context, error) {
	return MongoDBWrapper.GetDBContext(fd)
}

func Connect(config dbd.DB_Connection) (dbd.Db_id, error) {
	return MongoDBWrapper.Connect(config)
}

func IsConnected(fd dbd.Db_id) bool {
	return MongoDBWrapper.IsConnected(fd)
}

func Disconnect(fd dbd.Db_id) {
	MongoDBWrapper.Disconnect(fd)
}

func Get_documents(info *dbd.Db_Context, filter_list *[]dbd.Filters) ([]byte, error) {
	return MongoDBWrapper.Get_documents(info, filter_list)
}

func Get_document(info *dbd.Db_Context, filter_list *[]dbd.Filters) ([]byte, error) {
	return MongoDBWrapper.Get_document(info, filter_list)
}

func Insert_documents(info *dbd.Db_Context, a_payload any) ([]string, error) {
	return MongoDBWrapper.Insert_documents(info, a_payload)
}

func Insert_document(info *dbd.Db_Context, a_payload any) (string, error) {
	return MongoDBWrapper.Insert_document(info, a_payload)
}

func Delete_document(info *dbd.Db_Context, filter_list *[]dbd.Filters) (bool, error) {
	return MongoDBWrapper.Delete_document(info, filter_list)
}

func Delete_documents(info *dbd.Db_Context, filter_list *[]dbd.Filters) (bool, error) {
	return MongoDBWrapper.Delete_documents(info, filter_list)
}

func Update_documents(info *dbd.Db_Context, filter_list *[]dbd.Filters, update_Param string, update_value any) error {
	return MongoDBWrapper.Update_documents(info, filter_list, update_Param, update_value)
}

func ListConnections() []dbd.Db_id {
	return MongoDBWrapper.ListConnections()
}
