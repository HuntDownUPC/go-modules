package MongoDBWrapper

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strings"
	"time"

	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoContext struct {
	Client      *mongo.Client
	Collection  string
	Database    string
	isConnected bool
}

var ongoingConnections map[dbd.Db_id](mongoContext)

func init() {
	ongoingConnections = make(map[dbd.Db_id]mongoContext)
}

func ValidateSession(fd dbd.Db_id) bool {
	_, found := ongoingConnections[fd]
	return found
}

func GetNewFD() dbd.Db_id {
	var n dbd.Db_id
	var found bool = true
	for found {
		n = dbd.Db_id(rand.Intn(1000000))
		_, found = ongoingConnections[n]
	}
	return n
}

func GetClient(fd dbd.Db_id) *mongo.Client {
	if _, found := ongoingConnections[fd]; !found {
		return nil
	}
	return ongoingConnections[fd].Client
}

func ListConnections() []dbd.Db_id {
	arr := make([]dbd.Db_id, len(ongoingConnections))
	var i int = 0

	for val := range ongoingConnections {
		arr[i] = val
		i++
	}
	return arr
}

func GetDBBackendName() string {
	return "mongodb"
}

func ProcessFiltersSingle(filter_list []dbd.Filters, query *bson.M) bool {
	var raw_id string
	for i := 0; i < len(filter_list); i++ {
		if (filter_list)[i].Filter == "_id" {
			raw_id = filter_list[i].Value.(string)
			objID, _ := primitive.ObjectIDFromHex(raw_id)
			(*query)[filter_list[i].Filter] = bson.M{filter_list[i].Op: objID}
		} else {
			(*query)[filter_list[i].Filter] = bson.M{filter_list[i].Op: filter_list[i].Value}
		}
	}
	return true
}

func ProcessFiltersMultipleD(filter_list []dbd.Filters, query *[]bson.D) bool {
	var raw_id string
	for i := 0; i < len(filter_list); i++ {
		if (filter_list)[i].Filter == "_id" {
			raw_id = filter_list[i].Value.(string)
			objID, _ := primitive.ObjectIDFromHex(raw_id)
			filter := bson.D{{Key: filter_list[i].Filter, Value: bson.D{{Key: filter_list[i].Op, Value: objID}}}}
			*query = append(*query, filter)
		} else {
			filter := bson.D{{Key: filter_list[i].Filter, Value: bson.D{{Key: filter_list[i].Op, Value: filter_list[i].Value}}}}
			*query = append(*query, filter)
		}
	}
	return true
}

func ProcessFiltersMultiple(filter_list []dbd.Filters, query *[]bson.M) bool {
	var raw_id string
	for i := 0; i < len(filter_list); i++ {
		if (filter_list)[i].Filter == "_id" {
			raw_id = filter_list[i].Value.(string)
			objID, _ := primitive.ObjectIDFromHex(raw_id)
			*query = append(*query, bson.M{"$match": bson.M{filter_list[i].Filter: bson.M{filter_list[i].Op: objID}}})
		} else {
			*query = append(*query, bson.M{"$match": bson.M{filter_list[i].Filter: bson.M{filter_list[i].Op: filter_list[i].Value}}})
		}
	}
	return true
}

func GetCollection(fd dbd.Db_id) string {
	if _, ok := ongoingConnections[fd]; !ok {
		return ""
	}
	return ongoingConnections[fd].Collection
}

func GetDatabase(fd dbd.Db_id) string {
	if _, ok := ongoingConnections[fd]; !ok {
		return ""
	}
	return ongoingConnections[fd].Database
}

func UseCollection(fd dbd.Db_id, col_name string) (bool, error) {
	var err error
	if entry, ok := ongoingConnections[fd]; ok {
		var collections []string
		db := GetClient(fd).Database(entry.Database)
		if db == nil {
			err = errors.New("first initialize the database")
			return false, err
		}
		collections, err = GetClient(fd).Database(entry.Database).ListCollectionNames(context.TODO(), bson.D{})
		if err != nil {
			return false, err
		}
		for _, collection := range collections {
			if collection == col_name {
				entry.Collection = col_name
				ongoingConnections[fd] = entry
				return true, nil
			}
		}
		return false, errors.New("collection not found")
	} else {
		err = errors.New("connection to database not established")
		return false, err
	}
}

func UseDatabase(fd dbd.Db_id, db_name string) (bool, error) {
	var err error
	if entry, ok := ongoingConnections[fd]; ok {
		db := GetClient(fd).Database(db_name)
		if db == nil {
			err = errors.New("database not found")
			return false, err
		}
		entry.Database = db_name
		ongoingConnections[fd] = entry
		return true, nil
	} else {
		err = errors.New("connection to database not established")
		return false, err
	}
}

func GetDBContext(fd dbd.Db_id) (dbd.Db_Context, error) {
	var used_context dbd.Db_Context

	if tmp_context, found := ongoingConnections[fd]; found {
		used_context.Fd = fd
		used_context.Database = tmp_context.Database
		used_context.Collection = tmp_context.Collection
		return used_context, nil
	} else {
		return used_context, errors.New("context not found")
	}
}

func IsConnected(fd dbd.Db_id) bool {
	if cntx, found := ongoingConnections[fd]; found {
		return cntx.isConnected
	}
	return false
}

func Connect(config dbd.DB_Connection) (dbd.Db_id, error) {
	var mContext mongoContext
	var clientOptions *options.ClientOptions
	credential := options.Credential{
		Username: config.Username,
		Password: config.Password,
	}
	//check authentication
	clientOptions = options.Client().ApplyURI("mongodb://" + config.Host + ":" + config.Port).SetAuth(credential)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err_path := mongo.Connect(ctx, clientOptions)
	if err_path != nil {
		log.Println("Error connecting to database: ", err_path.Error())
		return -1, err_path
	}
	// Check the connection
	err_path = client.Ping(context.TODO(), nil)
	if err_path != nil {
		return -1, err_path
	}
	var n dbd.Db_id = GetNewFD()
	mContext.Client = client
	mContext.isConnected = true
	mContext.Database = client.Database("").Name()
	ongoingConnections[n] = mContext
	return n, nil
}

func Change_index(info *dbd.Db_Context, index string) {
	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	mod := mongo.IndexModel{
		Keys: bson.M{
			index: 1, // index in ascending order
		}, Options: options.Index().SetUnique(true),
	}
	ind, err := collection.Indexes().CreateOne(context.TODO(), mod)
	if err != nil {
		log.Println("Indexes().CreateOne() ERROR:, it may already exist", err)
	} else {
		// API call returns string of the index name
		log.Println("CreateOne() index:", ind)
	}
}

func Disconnect(fd dbd.Db_id) {
	var client *mongo.Client = GetClient(fd)
	if client == nil {
		log.Println("Some problem with logout happened")
		return
	}
	err := client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}
	delete(ongoingConnections, fd)
}

func Get_documents(info *dbd.Db_Context, filter_list *[]dbd.Filters) ([]byte, error) {
	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	var query []bson.M
	ProcessFiltersMultiple(*filter_list, &query)

	cursor, err := collection.Aggregate(context.TODO(), query)
	if err != nil {
		log.Println(err)
		return []byte(""), err
	}
	var docsFiltered []bson.M
	if err = cursor.All(context.TODO(), &docsFiltered); err != nil {
		log.Println(err)
		return []byte(""), err
	}

	p, err := json.Marshal(docsFiltered)
	if err != nil {
		log.Println(err)
		return p, err
	}
	return p, nil
}

func Get_document(info *dbd.Db_Context, filter_list *[]dbd.Filters) ([]byte, error) {
	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	query := bson.M{}
	ProcessFiltersSingle(*filter_list, &query)
	var result bson.M
	err := collection.FindOne(context.TODO(), query).Decode(&result)
	if err != nil {
		//no files found
		log.Println(err)
		return []byte(""), err
	}
	p, err := json.Marshal(result)
	if err != nil {
		log.Fatal(err)
	}
	return p, nil
}

func Insert_documents(info *dbd.Db_Context, rcv_document any) ([]string, error) {
	var oid_list []string
	var raw_payload []byte
	var j_payload []interface{}

	switch v := rcv_document.(type) {
	case int:
		log.Println("Error in insert_document payload must be JSON")
		return oid_list, errors.New("error in insert_document payload must be json")
	case string:
		err := json.Unmarshal([]byte(v), &j_payload)
		if err != nil {
			return oid_list, err
		}
	case []byte:
		raw_payload = v
	default:
		raw_payload, _ = json.Marshal(rcv_document)
	}
	marshal_error := json.Unmarshal(raw_payload, &j_payload)
	if marshal_error != nil {
		log.Println("Input error, not JSON format ", marshal_error)
	}

	// Remove _id from payload in case they exist to avoid duplicates
	for _, element := range j_payload {
		p, ok := element.(map[string]interface{})
		if !ok {
			log.Fatal("Invalid JSON array")
		}
		delete(p, "_id")
	}

	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	result, err := collection.InsertMany(context.TODO(), j_payload)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate") {
			payload, _ := json.Marshal(j_payload)
			log.Println("Duplicated element: " + string(payload))
		} else {
			log.Println(err)
		}
		return oid_list, err
	}
	for _, element := range result.InsertedIDs {
		log.Printf("Element Inserted with ID %v\n", element)
		var oid string
		tmp := strings.Split(fmt.Sprintf("%v", element), "\"")
		if len(tmp) > 1 {
			oid = tmp[1]
		} else {
			oid = tmp[0]
		}
		oid_list = append(oid_list, oid)
	}
	return oid_list, nil
}
func Insert_document(info *dbd.Db_Context, a_payload any) (string, error) {
	var b_payload []byte
	j_payload := make(map[string]interface{})

	switch v := a_payload.(type) {
	case int:
		log.Println("Error in insert_document payload must be JSON")
		return "", errors.New("error in insert_document payload must be json")
	case string:
		err := json.Unmarshal([]byte(v), &j_payload)
		if err != nil {
			return "", err
		}
	case []byte:
		b_payload = v
		log.Println(string(b_payload))
	default:
		b_payload, _ = json.Marshal(a_payload)
	}
	marshal_error := json.Unmarshal(b_payload, &j_payload)
	if marshal_error != nil {
		log.Println("Input error, not JSON format ", marshal_error)
	}
	delete(j_payload, "_id")

	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	result, err := collection.InsertOne(context.TODO(), j_payload)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate") {
			payload, _ := json.Marshal(j_payload)
			log.Println("Duplicated element: " + string(payload))
		} else {
			log.Println(err)
		}
		return "", err
	}
	log.Printf("Single Element Inserted with ID %v\n", result.InsertedID)
	var oid string
	tmp := strings.Split(fmt.Sprintf("%v", result.InsertedID), "\"")
	if len(tmp) > 1 {
		oid = tmp[1]
	} else {
		oid = tmp[0]
	}
	return oid, nil
}

func Delete_document(info *dbd.Db_Context, filter_list *[]dbd.Filters) (bool, error) {
	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	filter := bson.M{}
	ProcessFiltersSingle(*filter_list, &filter)
	res, err := collection.DeleteOne(ctx, filter)
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	log.Printf("%v document removed\n", res.DeletedCount)
	return true, nil //succeeded to delete
}

func Delete_documents(info *dbd.Db_Context, filter_list *[]dbd.Filters) (bool, error) {
	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	query := []bson.D{}
	ProcessFiltersMultipleD(*filter_list, &query)

	res, err := collection.DeleteMany(ctx, query)
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	log.Printf("%v document(s) removed\n", res.DeletedCount)
	return true, nil //succeeded to delete
}

func Update_documents(info *dbd.Db_Context, filter_list *[]dbd.Filters, update_Param string, update_value any) error {
	collection := GetClient(info.Fd).Database((*info).Database).Collection((*info).Collection)
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	query := bson.M{}
	ProcessFiltersSingle(*filter_list, &query)

	result, err := collection.UpdateMany(ctx, query, bson.D{
		{Key: "$set", Value: bson.D{{Key: update_Param, Value: update_value}}}})

	if err != nil {
		log.Println(err)
		return err //failed
	}
	log.Printf("Updated %v Documents!\n", result.ModifiedCount)
	return nil //update succeeded
}
