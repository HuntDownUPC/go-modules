package UsersMongoDB

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"gitlab.com/HuntDownUPC/go-modules/db/backends/mongo/MongoDBWrapper/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var admin_database string = "admin"
var general_database string = "general"

func initUserDb(fd dbd.Db_id, database string, username string) {
	var info dbd.Db_Context

	db_dummy := make(map[string]interface{})
	db_dummy["created"] = true

	info.Fd = fd
	info.Database = database
	info.Collection = username
	MongoDBWrapper.Insert_document(&info, db_dummy)
}

func Add_user(fd dbd.Db_id, path string) {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return
	}
	docsPath, _ := filepath.Abs(path)
	byteValues, err := os.ReadFile(docsPath)
	if err != nil {
		log.Fatal(err) //file doesn't exist
	}
	Add_users_direct(fd, byteValues)
}

func Add_one_user_direct(fd dbd.Db_id, byteValues []byte) {
	var doc dbd.User

	err := json.Unmarshal(byteValues, &doc)
	if err != nil {
		log.Fatal(err) //structure is bad
	}
	var rol []bson.M //array of roles that the user has
	for j := 0; j < len(doc.Roles); j++ {
		rol = append(rol, bson.M{"role": doc.Roles[j].Role, "db": doc.Roles[j].Db})
	}

	//we create user in admin database -> we use admin database to auth
	r := MongoDBWrapper.GetClient(fd).Database(admin_database).RunCommand(context.Background(), bson.D{{Key: "createUser", Value: doc.Username},
		{Key: "pwd", Value: doc.Password}, {Key: "roles", Value: rol}, {Key: "customData", Value: bson.M{"client": doc.Client}}})

	if r.Err() != nil {
		log.Printf("Error creating user %s \n", doc.Username)
		log.Println(r.Err())

	} else {
		log.Printf("User %s was created successfully \n", doc.Username)
	}
	log.Println("Creating dummy entry to bypass permissions")
	initUserDb(fd, doc.Client, doc.Username)
}

func Add_users_direct(fd dbd.Db_id, byteValues []byte) {
	var Docs []dbd.User

	err := json.Unmarshal(byteValues, &Docs)
	if err != nil {
		log.Fatal(err) //structure is bad
	}
	for i := range Docs {
		doc := Docs[i]

		var rol []bson.M //array of roles that the user has
		for j := 0; j < len(doc.Roles); j++ {
			rol = append(rol, bson.M{"role": doc.Roles[j].Role, "db": doc.Roles[j].Db})
		}

		//we create user in admin database -> we use admin database to auth
		r := MongoDBWrapper.GetClient(fd).Database(admin_database).RunCommand(context.Background(), bson.D{{Key: "createUser", Value: doc.Username},
			{Key: "pwd", Value: doc.Password}, {Key: "roles", Value: rol}, {Key: "customData", Value: bson.M{"client": doc.Client}}})

		if r.Err() != nil {
			log.Printf("Error creating user %s \n", doc.Username)
			log.Println(r.Err())

		} else {
			log.Printf("User %s was created successfully \n", doc.Username)
		}
		log.Println("Creating dummy entry to bypass permissions")
		initUserDb(fd, doc.Client, doc.Username)
	}
}

func Add_user_UI(fd dbd.Db_id, email string, password string, parent_id string) map[string]string {
	if !MongoDBWrapper.IsConnected(fd) {
		result := make(map[string]string)
		result["success"] = "false"
		log.Println("Error, database not connected")
		return result
	}
	//create ROL for user
	var act bson.A
	act = append(act, "find", "update", "insert")

	tmp, _ := strconv.Atoi(parent_id)
	var priv bson.A
	var clientUser string = GetUserDatabase(fd, MongoDBWrapper.GetCollection(dbd.Db_id(tmp)))

	priv = append(priv, bson.M{"resource": bson.M{"db": clientUser, "collection": email}, "actions": act})
	role := bson.A{} //no inherited rules

	r := MongoDBWrapper.GetClient(fd).Database(admin_database).RunCommand(context.Background(), bson.D{{Key: "createRole", Value: email},
		{Key: "privileges", Value: priv}, {Key: "roles", Value: role}})

	if r.Err() != nil {
		log.Println("error at runCommand: ", r.Err().Error())
		result := make(map[string]string)
		result["success"] = "false"
		result["username"] = email
		result["error"] = r.Err().Error()
		return result
	} else {
		log.Printf("Role %s was created successfully\n ", email)
	}

	var rol []bson.M //array of roles that the user has
	rol = append(rol, bson.M{"role": email, "db": admin_database})
	rol = append(rol, bson.M{"role": "read", "db": general_database})

	//we create user in admin database -> we use admin database to auth
	r = MongoDBWrapper.GetClient(fd).Database(admin_database).RunCommand(context.Background(), bson.D{{Key: "createUser", Value: email},
		{Key: "pwd", Value: password}, {Key: "roles", Value: rol}, {Key: "customData", Value: bson.M{"client": clientUser}}})

	if r.Err() != nil {
		log.Printf("Error creating user %s \n", email)
		log.Println(r.Err())
		result := make(map[string]string)
		result["success"] = "false"
		result["username"] = email
		result["error"] = r.Err().Error()
		return result
	} else {
		result := make(map[string]string)
		result["success"] = "true"
		result["username"] = email
		log.Println("Creating dummy entry to bypass permissions")
		initUserDb(fd, clientUser, email)
		log.Printf("User %s was created successfully \n", email)
		return result
	}
}

func DeleteRole(fd dbd.Db_id, role string) bool {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return false
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.roles")

	_, err := collection.DeleteOne(context.Background(), bson.M{"role": role})
	if err != nil {
		log.Fatal(err)
		return false
	}
	fmt.Printf("user %v removed ", role)
	return true //succeeded to delete

}

func DeleteUser(fd dbd.Db_id, user string) bool {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return false
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.users")

	_, err := collection.DeleteOne(context.Background(), bson.M{"user": user})
	if err != nil {
		log.Fatal(err)
		return false
	}
	log.Printf("user %v removed ", user)
	return true //succeeded to delete
}

func GetUserDatabase(fd dbd.Db_id, user string) string {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return ""
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.users")
	result := struct {
		User       string
		Db         string
		CustomData struct {
			Client string
		}
	}{}
	collection.FindOne(context.Background(), bson.D{{Key: "user", Value: user}}).Decode(&result)
	return result.CustomData.Client
}

func IsAdmin(fd dbd.Db_id, user string) bool {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return false
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.users")
	result := struct {
		User  string
		Db    string
		Roles []struct {
			Role string
			Db   string
		}
		//CustomData string
		CustomData struct {
			Client string
		}
	}{}
	collection.FindOne(context.Background(), bson.D{{Key: "user", Value: user}}).Decode(&result)
	for j := 0; j < len(result.Roles); j++ {
		if result.Roles[j].Role == "readWrite" && result.Roles[j].Db == result.CustomData.Client {
			return true
		}
	}
	return false
}

func GetUsersAdmin(fd dbd.Db_id, clientUser string) []byte {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return []byte("")
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.users")
	var query []bson.M
	query = append(query, bson.M{"$match": bson.M{"customData.client": bson.M{"$eq": clientUser}}})

	cursor, err := collection.Aggregate(context.TODO(), query)
	if err != nil {
		log.Fatal(err)
	}
	var docsFiltered []bson.M
	if err = cursor.All(context.TODO(), &docsFiltered); err != nil {
		log.Fatal(err)
	}

	p, err := json.Marshal(docsFiltered)
	if err != nil {
		log.Fatal(err)
	}

	return p
}
func GetUsers(fd dbd.Db_id) []byte {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return []byte("")
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.users")
	var query []bson.M
	query = append(query, bson.M{"$match": bson.M{"customData.client": bson.M{"$eq": MongoDBWrapper.GetDatabase(fd)}}})

	cursor, err := collection.Aggregate(context.TODO(), query)
	if err != nil {
		log.Fatal(err)
	}
	var docsFiltered []bson.M
	if err = cursor.All(context.TODO(), &docsFiltered); err != nil {
		log.Fatal(err)
	}

	p, err := json.Marshal(docsFiltered)
	if err != nil {
		log.Fatal(err)
	}

	return p
}

func UserExists(fd dbd.Db_id, email string) (bool, error) {
	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return false, errors.New("database not connected")
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.users")
	result := struct {
		User       string
		Db         string
		CustomData struct {
			Client string
		}
	}{}
	if err := collection.FindOne(context.Background(), bson.D{{Key: "user", Value: email}}).Decode(&result); err != nil {
		return false, err
	}
	return true, nil

}

func UserExistsComplete(fd dbd.Db_id, email string) map[string]string {
	if !MongoDBWrapper.IsConnected(fd) {
		status := make(map[string]string)
		log.Println("Error, database not connected")
		status["success"] = "false"
		return status
	}
	collection := MongoDBWrapper.GetClient(fd).Database(admin_database).Collection("system.users")
	result := struct {
		User       string
		Db         string
		CustomData struct {
			Client string
		}
	}{}
	status := make(map[string]string)
	status["username"] = email
	if err := collection.FindOne(context.Background(), bson.D{{Key: "user", Value: email}}).Decode(&result); err != nil {
		status["success"] = "false"
		status["error"] = err.Error()
		return status
	}
	status["success"] = "true"
	return status

}

func Add_role(fd dbd.Db_id, path string, database string) { //path of the json file with the roles
	docsPath, _ := filepath.Abs(path)
	byteValues, err := os.ReadFile(docsPath)
	if err != nil {
		log.Fatal(err) //file doesn't exist
		return
	}
	var Docs []dbd.Roles

	err = json.Unmarshal(byteValues, &Docs)
	if err != nil {
		log.Fatal(err) //structure is bad
		return
	}
	for i := range Docs {
		doc := Docs[i]

		var priv bson.A
		for j := range doc.Privileges { //traverse all privileges

			var act bson.A
			for p := range doc.Privileges[j].Actions {
				act = append(act, doc.Privileges[j].Actions[p])
			}
			priv = append(priv, bson.M{"resource": bson.M{"db": doc.Privileges[j].Resource.Db, "collection": doc.Privileges[j].Resource.Collection}, "actions": act})

		}
		if len(doc.Privileges) == 0 {
			priv = bson.A{} //no privileges
		}
		var rol bson.A
		for t := range doc.Roles {
			rol = append(rol, bson.M{"role": doc.Roles[t].Role, "db": doc.Roles[t].Db})
		}
		if len(doc.Roles) == 0 {
			rol = bson.A{} //no inherited rules
		}
		//r := client.Database(admin_database).RunCommand(context.Background(), bson.D{{"createRole", "newRol"},	{"privileges", bson.A{ bson.M{ "resource": bson.M{"db": "Cluster0", "collection": "",},"actions": bson.A{"insert","dbStats","collStats","compact",},},}}, {"roles", bson.A{bson.M{"role": "readWrite", "db": "Cluster0",},},}})
		r := MongoDBWrapper.GetClient(fd).Database(database).RunCommand(context.Background(), bson.D{{Key: "createRole", Value: doc.CreateRole},
			{Key: "privileges", Value: priv}, {Key: "roles", Value: rol}})
		if r.Err() != nil {
			log.Println("Error at runCommand")
			log.Println(r.Err())
			return
		} else {
			log.Printf("Role %s was created successfully\n ", doc.CreateRole)
		}
	}
}

func GetAllRoles(fd dbd.Db_id) ([]string, error) {
	var database string = "admin"
	var col string = "system.users"

	if !MongoDBWrapper.IsConnected(fd) {
		log.Println("Error, database not connected")
		return []string{}, errors.New("database not connected")
	}
	collection := MongoDBWrapper.GetClient(fd).Database(database).Collection(col)

	pipeline := []bson.M{
		bson.M{"$addFields": bson.M{
			"dbValues": bson.M{"$map": bson.M{
				"input": "$roles",
				"as":    "role",
				"in":    "$$role.db",
			}},
		}},
		bson.M{"$project": bson.M{
			"_id":      0,
			"dbValues": 1,
		}},
	}

	cur, err := collection.Aggregate(context.TODO(), pipeline)
	if err != nil {
		log.Println("Error at aggregate", err)
		return []string{}, err
	}

	var result bson.M
	var client_list []string
	re := regexp.MustCompile(`client(\d+)`)
	for cur.Next(context.TODO()) {
		err := cur.Decode(&result)
		if err != nil {
			fmt.Println(err)
			return []string{}, err
		}
		input := result["dbValues"].(primitive.A)[0].(string)
		matches := re.FindAllStringSubmatch(input, -1)
		if len(matches) > 0 && len(matches[0]) > 1 {
			client_list = append(client_list, input)
		}
	}
	err = cur.Close(context.TODO())
	if err != nil {
		fmt.Println(err)
		return []string{}, err
	}
	return client_list, nil
}
