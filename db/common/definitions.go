package common

type Db_id int

// TODO remove mongo dependency from here
type DB_Client struct {
	Fd Db_id
}

type Filters struct {
	Filter string
	Value  interface{}
	Op     string
}

type DB_Connection struct {
	Username string
	Password string
	Host     string
	Port     string
}

type Db_Context struct {
	Fd         Db_id
	Database   string
	Collection string
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Roles    []Role `json:"roles"`
	Client   string `json:"client"`
}

type Users struct {
	User  string `json:"user"`
	Db    string `json:"db"`
	Roles []struct {
		Role string `json:"role"`
		Db   string `json:"db"`
	} `json:"roles"`
	//CustomData string
	CustomData struct {
		Client string `json:"client"`
	} `json:"customData"`
}

type Role struct { //to add roles to the user
	Role string `json:"role"`
	Db   string `json:"db"`
}

type Roles struct { //to create new roles
	CreateRole string `json:"createRole"`
	//db string `json:"db"`
	Privileges []Privilege `json:"privileges"`
	Roles      []Role      `json:"roles"`
}

type Privilege struct {
	Resource struct {
		Db         string `json:"db"`
		Collection string `json:"collection"`
	} `json:"resource"`
	Actions []string `json:"actions"`
}
