package DBUsers

import (
	mongoUsers "gitlab.com/HuntDownUPC/go-modules/db/backends/mongo/UsersMongoDB/v2"
	dbd "gitlab.com/HuntDownUPC/go-modules/db/common/v2"
)

func Add_one_user_direct(fd dbd.Db_id, byteValues []byte) {
	mongoUsers.Add_one_user_direct(fd, byteValues)
}

func Add_users_direct(fd dbd.Db_id, byteValues []byte) {
	mongoUsers.Add_users_direct(fd, byteValues)
}

func Add_user(fd dbd.Db_id, path string) {
	mongoUsers.Add_user(fd, path)
}

func Add_user_UI(fd dbd.Db_id, email string, password string, parent_id string) map[string]string {
	return mongoUsers.Add_user_UI(fd, email, password, parent_id)
}

func DeleteRole(fd dbd.Db_id, role string) bool {
	return mongoUsers.DeleteRole(fd, role)
}

func DeleteUser(fd dbd.Db_id, user string) bool {
	return mongoUsers.DeleteUser(fd, user)
}

func GetUserDatabase(fd dbd.Db_id, user string) string {
	return mongoUsers.GetUserDatabase(fd, user)
}

func IsAdmin(fd dbd.Db_id, user string) bool {
	return mongoUsers.IsAdmin(fd, user)
}

func GetUsersAdmin(fd dbd.Db_id, clientUser string) []byte {
	return mongoUsers.GetUsersAdmin(fd, clientUser)
}

func GetUsers(fd dbd.Db_id) []byte {
	return mongoUsers.GetUsers(fd)
}

func UserExists(fd dbd.Db_id, email string) (bool, error) {
	return mongoUsers.UserExists(fd, email)
}

func UserExistsComplete(fd dbd.Db_id, email string) map[string]string {
	return mongoUsers.UserExistsComplete(fd, email)
}

func Add_role(fd dbd.Db_id, path string, database string) {
	mongoUsers.Add_role(fd, path, database)
}

func GetAllRoles(fd dbd.Db_id) ([]string, error) {
	return mongoUsers.GetAllRoles(fd)
}
