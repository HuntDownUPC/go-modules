package PulsarLib

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"
)

type MessageResponse struct {
	Id  string `json:"id"`
	Msg map[string]any
}

type PL_Client struct {
	Client *pulsar.Client
}

func BuildMessage(msg map[string]any) *MessageResponse {
	m := MessageResponse{Msg: msg}
	m.Id = ""
	return &m
}

//Default configuration URL: "pulsar://localhost:6650"

func InitClient(URL string) PL_Client {
	var pl_client PL_Client
	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL:               URL,
		OperationTimeout:  30 * time.Second,
		ConnectionTimeout: 30 * time.Second,
	})
	if err != nil {
		log.Fatalf("Could not instantiate Pulsar client: %v", err)
	}
	pl_client.Client = &client
	return pl_client
}

func WaitEvent(pb_client PL_Client, topic string, callback func([]byte)) {
	client := *pb_client.Client
	go func() {
		consumer, err := client.Subscribe(pulsar.ConsumerOptions{
			Topic:            topic,
			SubscriptionName: topic,
			Type:             pulsar.Exclusive,
		})
		if err != nil {
			log.Fatal(err)
		}
		defer consumer.Close()
		for {
			var msg pulsar.Message
			msg, err = consumer.Receive(context.Background())
			if err != nil {
				log.Println(err)
				break
			}

			go callback(msg.Payload())
			consumer.Ack(msg)
		}
		//Destroy consumer
		if err := consumer.Unsubscribe(); err != nil {
			log.Fatal(err)
		}
	}()
}

func SendRequestSync(pb_client PL_Client, topic string, message *MessageResponse) []byte {
	client := *pb_client.Client
	id := rand.Intn(10000)
	topicId := topic + "." + strconv.Itoa(id)
	message.Id = topicId
	msg, _ := json.Marshal(message)
	consumer, err := client.Subscribe(pulsar.ConsumerOptions{
		Topic:            topicId,
		SubscriptionName: topicId,
		Type:             pulsar.Exclusive,
	})
	if err != nil {
		log.Fatal(err)
	}

	producer, err := client.CreateProducer(pulsar.ProducerOptions{
		Topic: topic,
	})
	if err != nil {
		log.Fatal(err)
	}
	_, err = producer.Send(context.Background(), &pulsar.ProducerMessage{
		Payload: msg,
	})

	if err != nil {
		fmt.Println("Failed to publish message", err)
	}
	fmt.Println("Published message" + string(msg))

	res, err := consumer.Receive(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	//Destroy consumer
	if err := consumer.Unsubscribe(); err != nil {
		log.Fatal(err)
	}
	return res.Payload()
}

func SendRequestAsync(pb_client PL_Client, topic string, message *MessageResponse, foo func([]byte)) {
	client := *pb_client.Client
	id := rand.Intn(10000)
	topicId := topic + "." + strconv.Itoa(id)
	message.Id = topicId
	msg, _ := json.Marshal(message)

	consumer, err := client.Subscribe(pulsar.ConsumerOptions{
		Topic:            topicId,
		SubscriptionName: topicId,
		Type:             pulsar.Exclusive,
	})
	if err != nil {
		log.Fatal(err)
	}

	producer, err := client.CreateProducer(pulsar.ProducerOptions{
		Topic: topic,
	})
	if err != nil {
		log.Fatal(err)
	}
	_, err = producer.Send(context.Background(), &pulsar.ProducerMessage{
		Payload: msg,
	})

	if err != nil {
		fmt.Println("Failed to publish message", err)
	}
	fmt.Println("Published message " + string(msg))

	res, err := consumer.Receive(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	//Destroy consumer
	if err := consumer.Unsubscribe(); err != nil {
		log.Fatal(err)
	}
	go foo(res.Payload())

}

func SendMessage(pb_client PL_Client, topic string, message []byte) {
	client := *pb_client.Client
	producer, err := client.CreateProducer(pulsar.ProducerOptions{
		Topic: topic,
	})
	if err != nil {
		log.Println(err)
		return
	}
	_, err = producer.Send(context.Background(), &pulsar.ProducerMessage{
		Payload: message,
	})
	if err != nil {
		fmt.Println("Failed to publish message", err)
	}
	fmt.Println("Published message " + string(message))
}
